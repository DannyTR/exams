CREATE DATABASE PET;
USE PET;

CREATE TABLE Mascota(
	IdMascota     int not null primary key identity(1,1),
	NombreMascota nvarchar(150),
	RazaMascota   nvarchar(150),
	EdadMascota   int,
	NumeroFicha   int
);