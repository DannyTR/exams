<?php

	include("conexion.php");

	//crear consulta para listar datos
	$consulta = "SELECT
						IdCandy,
						Nombre,
						DulcesPedido,
						Cantidad,
						Total
						FROM candy";

	$ejecuta = $conexion -> query($consulta) or die("Error al listar productos <br> :" . $conexion -> error);				


?>

<!DOCTYPE html>
<html lang="es">
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Datos del pedido</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>PEDIDOS</h2>
			<hr />


			<div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
                    <th>NO</th>
					<th>NOMBRE</th>
					<th>DULCES</th>
                    <th>CANTIDAD</th>
                    <th>PRECIO</th>
                    <th colspan="2">ACCIONES</th>
				</tr>
		

		<?php

		while( $arreglo_resultados = $ejecuta -> fetch_row() ){
		echo '<tr>';
		echo '<td>' . $arreglo_resultados[0] . '</td>';
		echo '<td>' . $arreglo_resultados[1] . '</td>';
		echo '<td>' . $arreglo_resultados[2] . '</td>';
		echo '<td>' . $arreglo_resultados[3] . '</td>';
		echo '<td>' . $arreglo_resultados[4] . '</td>';
		//botón para editar
		echo '<td> <button type="button" onclick="formPedido('.$arreglo_resultados[0].');" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>';
		echo '</button></td>';
		//botón eliminar
		echo '<td> <button type="button" onclick="eliminarPedido('.$arreglo_resultados[0].');" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>';
		echo '</button></td>';

		echo '</tr>';
	}

	?>

		</table>
		</div>

</body>
</html>

