<?php
//incluye conexion a BD
	include("conexion.php");

	if (isset($_GET['id'])) {
	 	
	 	$llave = $_GET['id'];

	$consulta = "SELECT
					IdCandy,
					Nombre,
					DulcesPedido,
					Cantidad,
					Total
				FROM candy
				WHERE IdCandy = $llave";
	$ejecuta = $conexion -> query($consulta) or die("Error al consultar datos del pedido: " . $conexion -> error);
	$datos = $ejecuta -> fetch_row();		

	 }else{
	 	$datos[0]='';
		$datos[1]='';
		$datos[2]='';
		$datos[3]='';
		$datos[4]='';
	 }  

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!-- Bootstrap -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="../css/style_nav.css" rel="stylesheet">
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include("nav.php");?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>Editar pedido</h2>
			<hr />

	<form action="saveEdition.php" class="form-horizontal" method="POST">
	<table>
		<tr>
			<td>No: </td>
			<td>
				<input type="number" name="IdCandy" value="<?php echo $datos[0];?>" class="form-control" readonly > 
			</td>
		</tr>
		<tr>
			<td>Nombre: </td>
			<td>
				<input type="text" name="Nombre" value="<?php echo $datos[1];?>" class="form-control"> 
			</td>
		</tr>
			<td>Dulces: </td>
			<td>
				<input type="text" name="DulcesPedido" value="<?php echo $datos[2];?>" class="form-control"> 
			</td>
		</tr>
		<tr>
			<td>Cantidad: </td>
			<td>
				<input type="number" name="Cantidad" value="<?php echo $datos[3];?>" class="form-control"> 
			</td>
		</tr>
		<tr>
			<td>Precio: </td>
			<td>
				<input type="number" name="Total" value="<?php echo $datos[4];?>" class="form-control"> 
			</td>
			
		</tr>

		<tr>
			<td colspan="2" align="center">
				<br><button type="submit" class="btn btn-sm btn-primary">Guardar</button>
			</td>
		</tr>
	</table>
</form>

</body>
</html>

