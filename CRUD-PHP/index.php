<?php
?>	

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CRUD Dulcería</title>

	<!--Incluimos librería JQuery-->
	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
	<!--Incluimos nuestras funciones JS-->
	<script type="text/javascript" src="js/mainFunctions.js"></script>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style_nav.css" rel="stylesheet">

	<style>
		.content {
			margin-top: 80px;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<?php include('php/nav.php');?>
	</nav>
	<div class="container">
		<div class="content">
			<h2>J's & D's Candy Store</h2>
			<hr />

	<!--Aqui se enlistan los pedidos-->		
	<div id="contenido_lista">
		<?php include("php/listCandy.php");?>
	</div>

	<!--Botón para agregar un nuevo pedido-->
	<button type="button" onclick="nuevoPedido();" class="btn btn-sm btn-success">Agregar</button>

</body>
</html> 