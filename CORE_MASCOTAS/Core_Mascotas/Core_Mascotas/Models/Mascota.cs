﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Mascotas.Models
{
    public class Mascota
    {
        [Key]
        public int IdMascota { get; set; }

        [Required(ErrorMessage = "Ingrese el nombre de la mascota")]
        [Display(Name = "Nombre de la mascota")]
        public string NombreMascota { get; set; }

        [Required(ErrorMessage = "Ingrese la raza")]
        [Display(Name = "Raza")]
        public string RazaMascota { get; set; }

        [Required(ErrorMessage = "Ingrese la edad")]
        [Display(Name = "Edad")]
        [Range(0, 25)]
        public int EdadMascota { get; set; }

        [Required(ErrorMessage = "Ingrese número de ficha")]
        [Display(Name = "No. Ficha")]
        [Range(0, 25)]
        public int NumeroFicha { get; set; }

        [Required(ErrorMessage = "Ingrese el nombre del veterinario que atendera")]
        [Display(Name = "Veterinario")]
        public string Vet { get; set; }
    }
}
