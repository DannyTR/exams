﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core_Mascotas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Core_Mascotas.Controllers
{
    public class MascotaController : Controller
    {
        private readonly ApplicationDbContext _db;
        public MascotaController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index() // 1) Crear vista
        {
            var displaydata = _db.Mascota.ToList();
            return View(displaydata);
        }

        [HttpGet] //Barra de busqueda
        public async Task<IActionResult> Index(string petSearch)
        {
            ViewData["GetMascotaDetails"] = petSearch;
            var petquery = from x in _db.Mascota select x;
            if (!String.IsNullOrEmpty(petSearch))
            {
                petquery = petquery.Where(x => x.NombreMascota.Contains(petSearch) || x.Vet.Contains(petSearch));
            }
            return View(await petquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }


        //2)Crear registro
        [HttpPost]
        public async Task<IActionResult> Create(Mascota nPet)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nPet);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(nPet);
        }

        //3) Detalles del resgitro
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPetDetail = await _db.Mascota.FindAsync(id);
            return View(getPetDetail);
        }

        //4)Editar cliente
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPetDetail = await _db.Mascota.FindAsync(id);
            return View(getPetDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Mascota oldPet)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldPet);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldPet);
        }

        //5)Eliminar cliente
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getPetDetails = await _db.Mascota.FindAsync(id);
            return View(getPetDetails);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getPetdetail = await _db.Mascota.FindAsync(id);
            _db.Mascota.Remove(getPetdetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}
